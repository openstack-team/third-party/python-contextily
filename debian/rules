#!/usr/bin/make -f

UPSTREAM_GIT := https://github.com/geopandas/contextily
include /usr/share/openstack-pkg-tools/pkgos.make

export SETUPTOOLS_SCM_PRETEND_VERSION=$(shell dpkg-parsechangelog -SVersion | sed -e 's/^[[:digit:]]*://' -e 's/[-].*//' -e 's/~git.*//' -e 's/~bpo.*//' -e 's/~/.0/' -e 's/+dfsg1//' -e 's/+ds1//' | head -n 1)

PYBUILD_NAME=contextily
export PYBUILD_VERBOSE=1

export MPLCONFIGDIR=/tmp

%:
	dh $@ --buildsystem=pybuild --with python3,sphinxdoc

override_dh_auto_clean:
	rm -rf build .stestr *.egg-info
	rm -f	docs/README.rst \
		docs/add_basemap_deepdive.ipynb \
		docs/friends_cenpy_osmnx.ipynb \
		docs/friends_gee.ipynb \
		docs/intro_guide.ipynb \
		docs/places_guide.ipynb \
		docs/providers_deepdive.ipynb \
		docs/tiles.png \
		docs/warping_guide.ipynb \
		docs/working_with_local_files.ipynb
	find . -iname '*.pyc' -delete
	for i in $$(find . -type d -iname __pycache__) ; do rm -rf $$i ; done
	dh_auto_clean

override_dh_sphinxdoc:
ifeq (,$(findstring nodoc, $(DEB_BUILD_OPTIONS)))
	PYTHONPATH=. python3 -m sphinx -b html docs debian/python-contextily-doc/usr/share/doc/python-contextily-doc/html
	find $(CURDIR)/debian/python-contextily-doc/usr/share/doc/python-contextily-doc/html/ -iname '*.pyc' -delete
	for i in $$(find $(CURDIR)/debian/python-contextily-doc/usr/share/doc/python-contextily-doc/html -type d -iname __pycache__) ; do rm -rf $$i ; done
	dh_sphinxdoc
endif

override_dh_auto_test:
	echo "Disable all tests, as they are doing outbound connections."
