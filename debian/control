Source: python-contextily
Section: python
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper (>= 10),
 dh-python,
 openstack-pkg-tools,
 pybuild-plugin-pyproject,
 python3-all,
 python3-setuptools,
 python3-setuptools-scm,
 python3-sphinx,
Build-Depends-Indep:
 python3-geopy,
 python3-joblib,
 python3-matplotlib,
 python3-mercantile,
 python3-nbsphinx,
 python3-numpydoc,
 python3-pil,
 python3-pypandoc,
 python3-pytest,
 python3-rasterio,
 python3-requests,
 python3-sphinx-rtd-theme,
 python3-xyzservices,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/openstack-team/python/python-contextily
Vcs-Git: https://salsa.debian.org/openstack-team/python/python-contextily.git
Homepage: https://github.com/geopandas/contextily

Package: python3-contextily
Architecture: all
Depends:
 python3-geopy,
 python3-joblib,
 python3-matplotlib,
 python3-mercantile,
 python3-pil,
 python3-rasterio,
 python3-requests,
 python3-xyzservices,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-contextily-doc,
Description: Context geo-tiles in Python
 Contextily is a package to retrieve tile maps from the internet. It can add
 those tiles as basemap to matplotlib figures or write tile maps to disk into
 geospatial raster files. Bounding boxes can be passed in both WGS84
 (EPSG:4326) and Spheric Mercator (EPSG:3857).

Package: python-contextily-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Description: Context geo-tiles in Python - doc
 Contextily is a package to retrieve tile maps from the internet. It can add
 those tiles as basemap to matplotlib figures or write tile maps to disk into
 geospatial raster files. Bounding boxes can be passed in both WGS84
 (EPSG:4326) and Spheric Mercator (EPSG:3857).
 .
 This package contains the documentation.
